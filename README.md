# Implementation Decisions
**Libraries/Frameworks:**

* For dependency injection I've used Hilt as it is the recommended approach by Google and provides a simpler and more opinionated solution over Dagger.
* For the database I choose to use Room as it's also the widely accepted best approach for ORM on Android.
* I've implemented the UI layer using Jetpack Compose as it provides many advantages IMO over tradditional layouts, particulaly when it comes to list, where there's no need for writing Adapters, Viewholders or even DIffUtil Callbacks.
* Image loading is done with Coil, as it's a very simple to implement, written in Kotlin and lighter that traditional libs like Glide or Picasso.
* Threading is done with coroutines, as it's the recommended approach and integrates nicely with jetpack compose and components like the viewmodel
* Unit tests are done with the help of junit, mockito for mocking test dependencies and turbine to asert flow emissions

**Implementation Choices**

* Regarding the architectural pattern required for view interaction I choose to use MVVM. Reason for this is this is the recommended pattern from Google and provides many advantages over older patterns such as MVP or even MVC, such as built-in lifecycle management.
* As for the remainning code layers, I choose to use Uncle Bob's Clean Architecture, as it provides good principles for abtraction of external dependencies, separation between different layers, and so on. As such, this is structured with the following:
	*  **MessageService** - This is just fake message service that simulates a delay when sending a message and additionally "receives" a random message from the destination. In a real scenario, this message would otherwise be received by some sort of web socket implemenation
	*  **ChatMessageRepository** - This component provides separation betweem the viewmodel and the messging service, such that a direct dependency doesn't exist. This is helpful if we need to switch the service to a different provided, in which case we would most likely only need to change the repository and domain mapper
	*  **DomainMapper** - This component translates service entities (API/Database/WebSocket/...) into domain entities, for the same reasons as the above. Such separation means that any changes upstream wouldnt most likely reflect on classes that depend on these external services
	*  **ChatUiMapper** - This class serves two purposes, holding all the logic necessary for the view (add tails, insert timestamp separators, ...) and also translates the information received into a ui model. Once again, this is helpfull as it provides separation from the domain model, in case it is also used by other features/screens, so that a change to one screen doesn't affect another
	*  **ViewModel**  - Deals with all the thread orchestration and is responsible for calling the above layers and managign those calls within the android lifecycle


# App Limitations
Feature-wise, I'd say there are a couple of limitations of the current implementation, for the sake of simplicity:

* There is no support for any kind of media, wether that be images, video, gif, etc..
* The app is not ready tu support group chats on either a database or ui level
* Back arrow and Profile pircture/name do not have any action, such as going back or opening more details


# Possible Improvements
### Technical:
1. **Modular Project**, creating modules as needed for database, api, different features, core code, other utils, and so on. This would allow better separation of concerns and improve build times for the project
2. **Standardized components**, such as buttons, input boxs and others, so that they could be re-used in other screens/features. Screenshot tests could also be added following, so that any regressions of these components is caught before a release
3. **UI Tests**, would allow us to verify that key interactions with the app produce the appropriate result, such as sending a message and asserting that it appears in the message list. 
4. **Integration Unit Tests**, so that we can assert if all the components of a feature work well together. This can be helpfull in catching issues like arguments passed in the wrong order on a function call (fun doSomething(value1:String, value2:String) being called with doSomething(value2, value1)
5. **Database Unit Tests**, to make sure all room operations are working as expected, including table operations and migrations, if any
6. **Improve database structure** in order to provide better support for group chats in the future, maybe by adding a Conversation entity that would reference all the participants and respective messages, either for single chat or group chat


### Features
1. Add Profile more details bottom sheet
2. Add support for sending media content

# Repository Link
[https://bitbucket.org/Nuno_Neto/muzmatch_task/src/master/
](https://bitbucket.org/Nuno_Neto/muzmatch_task/src/master/)


