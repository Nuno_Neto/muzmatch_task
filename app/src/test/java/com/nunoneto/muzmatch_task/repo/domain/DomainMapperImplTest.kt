package com.nunoneto.muzmatch_task.repo.domain

import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.utils.chatMessageDomain
import com.nunoneto.muzmatch_task.utils.chatMessageEntity
import com.nunoneto.muzmatch_task.utils.participantDomain
import com.nunoneto.muzmatch_task.utils.participantEntity
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DomainMapperImplTest {

    private lateinit var underTest: IDomainMapper

    @Before
    fun setup() {
        underTest = DomainMapperImpl()
    }

    @Test
    fun `given participant entity, should map to participant domain model`() {
        val actual = underTest.mapParticipant(participantEntity())

        assertEquals(participantDomain(), actual)
    }

    @Test
    fun `given sent chat message entity, should map to sent chat message domain model`() {
        val actual = underTest.mapChatMessage(chatMessageEntity())

        assertEquals(chatMessageDomain(), actual)
    }

    @Test
    fun `given received chat message entity, should map to received chat message domain model`() {
        val actual = underTest.mapChatMessage(chatMessageEntity().copy(direction = ChatMessageDomain.Direction.RECEIVED.value))

        assertEquals(chatMessageDomain().copy(direction = ChatMessageDomain.Direction.RECEIVED), actual)
    }
}
