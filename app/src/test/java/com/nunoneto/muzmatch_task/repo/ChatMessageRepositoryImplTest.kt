package com.nunoneto.muzmatch_task.repo

import app.cash.turbine.test
import com.nunoneto.muzmatch_task.db.MuzDatabase
import com.nunoneto.muzmatch_task.db.dao.ChatMessageDao
import com.nunoneto.muzmatch_task.db.dao.ParticipantDao
import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity
import com.nunoneto.muzmatch_task.repo.domain.IDomainMapper
import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.repo.domain.models.ParticipantDomain
import com.nunoneto.muzmatch_task.service.IMessageService
import com.nunoneto.muzmatch_task.utils.DateUtils
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.inOrder
import org.mockito.BDDMockito.verifyNoInteractions
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.Date

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class ChatMessageRepositoryImplTest {

    @Mock
    private lateinit var muzDatabase: MuzDatabase
    @Mock
    private lateinit var domainMapper: IDomainMapper
    @Mock
    private lateinit var dateUtils: DateUtils
    @Mock
    private lateinit var messagingService: IMessageService

    private lateinit var underTest: IChatMessageRepository

    @Before
    fun setup() {
        underTest = ChatMessageRepositoryImpl(
            muzDatabase = muzDatabase,
            domainMapper = domainMapper,
            dateUtils = dateUtils,
            messagingService = messagingService
        )
    }

    @Test
    fun `given participant exists in db, show return flow of participant domain`() = runTest {
        val participantId = "some_participant_id"

        val participantDao = mock(ParticipantDao::class.java)
        val participantEntity = mock(ParticipantEntity::class.java)
        val participantModel = mock(ParticipantDomain::class.java)

        given(muzDatabase.participantDao()).willReturn(participantDao)
        given(participantDao.getParticipant(participantId)).willReturn(flowOf(participantEntity))
        given(domainMapper.mapParticipant(participantEntity)).willReturn(participantModel)

        underTest.getParticipant(participantId = participantId).test {
            assertEquals(participantModel,awaitItem())
            awaitComplete()
        }

        inOrder(participantDao, domainMapper).apply {
            verify(participantDao).getParticipant(participantId)
            verify(domainMapper).mapParticipant(participantEntity)
        }
    }

    @Test
    fun `given participant exists in db and mapper throws error, show return empty flow`() = runTest {
        val participantId = "some_participant_id"

        val participantDao = mock(ParticipantDao::class.java)
        val participantEntity = mock(ParticipantEntity::class.java)

        given(muzDatabase.participantDao()).willReturn(participantDao)
        given(participantDao.getParticipant(participantId)).willReturn(flowOf(participantEntity))
        given(domainMapper.mapParticipant(participantEntity)).willThrow(NullPointerException())

        underTest.getParticipant(participantId = participantId).test {
            awaitComplete()
        }

        inOrder(participantDao, domainMapper).apply {
            verify(participantDao).getParticipant(participantId)
            verify(domainMapper).mapParticipant(participantEntity)
        }
    }

    @Test
    fun `given participant doesnt exist in db , show return empty flow`() = runTest {
        val participantId = "some_participant_id"

        val participantDao = mock(ParticipantDao::class.java)

        given(muzDatabase.participantDao()).willReturn(participantDao)
        given(participantDao.getParticipant(participantId)).willReturn(emptyFlow())

        val actual = mutableListOf<ParticipantDomain>()
        underTest.getParticipant(participantId = participantId).test {
            awaitComplete()
        }

        assertEquals(emptyList<ParticipantDomain>(), actual)

        inOrder(participantDao, domainMapper).apply {
            verify(participantDao).getParticipant(participantId)
            verifyNoInteractions(domainMapper)
        }
    }

    @Test
    fun `given participant dao throws error , show return empty flow`() = runTest {
        val participantId = "some_participant_id"

        val participantDao = mock(ParticipantDao::class.java)

        given(muzDatabase.participantDao()).willReturn(participantDao)
        given(participantDao.getParticipant(participantId)).willReturn(flow { throw IllegalArgumentException() })

        underTest.getParticipant(participantId = participantId).test {
            awaitComplete()
        }

        inOrder(participantDao, domainMapper).apply {
            verify(participantDao).getParticipant(participantId)
            verifyNoInteractions(domainMapper)
        }
    }

    @Test
    fun `given messages exists in db, show return flow of domain message list `() = runTest {
        val participantId = "some_participant_id"

        val chatMessageDao = mock(ChatMessageDao::class.java)
        val messageEntity = mock(ChatMessageEntity::class.java)
        val messageDomain = mock(ChatMessageDomain::class.java)

        given(muzDatabase.chatMessageDao()).willReturn(chatMessageDao)
        given(chatMessageDao.observeMessagesForParticipant(participantId)).willReturn(flowOf(listOf(messageEntity)))
        given(domainMapper.mapChatMessage(messageEntity)).willReturn(messageDomain)

        underTest.observeChatMessages(participantId = participantId).test {
            assertEquals(listOf(messageDomain),awaitItem())
            awaitComplete()
        }

        inOrder(chatMessageDao, domainMapper).apply {
            verify(chatMessageDao).observeMessagesForParticipant(participantId)
            verify(domainMapper).mapChatMessage(messageEntity)
        }
    }

    @Test
    fun `given domain mapper throws error, show return empty flow of domain message list `() = runTest {
        val participantId = "some_participant_id"

        val chatMessageDao = mock(ChatMessageDao::class.java)
        val messageEntity = mock(ChatMessageEntity::class.java)

        given(muzDatabase.chatMessageDao()).willReturn(chatMessageDao)
        given(chatMessageDao.observeMessagesForParticipant(participantId)).willReturn(flowOf(listOf(messageEntity)))
        given(domainMapper.mapChatMessage(messageEntity)).willThrow(NullPointerException())

        underTest.observeChatMessages(participantId = participantId).test {
            assertEquals(emptyList<ChatMessageDomain>(),awaitItem())
            awaitComplete()
        }

        inOrder(chatMessageDao, domainMapper).apply {
            verify(chatMessageDao).observeMessagesForParticipant(participantId)
            verify(domainMapper).mapChatMessage(messageEntity)
        }
    }

    @Test
    fun `given messages dao throws error, show return empty flow of messages `() = runTest {
        val participantId = "some_participant_id"

        val chatMessageDao = mock(ChatMessageDao::class.java)

        given(muzDatabase.chatMessageDao()).willReturn(chatMessageDao)
        given(chatMessageDao.observeMessagesForParticipant(participantId)).willReturn(flow { throw NullPointerException() })

        underTest.observeChatMessages(participantId = participantId).test {
            assertEquals(emptyList<ChatMessageDomain>(),awaitItem())
            awaitComplete()
        }

        inOrder(chatMessageDao, domainMapper).apply {
            verify(chatMessageDao).observeMessagesForParticipant(participantId)
            verifyNoInteractions(domainMapper)
        }
    }

    @Test
    fun `given message is sent, should add to database and call message service`() = runTest {
        val message = "helllooooo!"
        val destination = "some_participant_id"

        val now = Date()
        given(dateUtils.getNow()).willReturn(now)
        val chatMessageDao = mock(ChatMessageDao::class.java)
        given(muzDatabase.chatMessageDao()).willReturn(chatMessageDao)

        underTest.sendMessage(message, destination)

        inOrder(messagingService, chatMessageDao).apply {
            verify(chatMessageDao).addMessage(ChatMessageEntity(
                content = message,
                participantId = destination,
                timestamp = now.time,
                direction = ChatMessageDomain.Direction.SENT.value
            ))

            verify(messagingService).sendMessage(message, destination)
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `given message is sent and dao throws error, should not call message service`() = runTest {
        val message = "helllooooo!"
        val destination = "some_participant_id"

        val now = Date()
        given(dateUtils.getNow()).willReturn(now)
        val chatMessageDao = mock(ChatMessageDao::class.java)
        given(muzDatabase.chatMessageDao()).willReturn(chatMessageDao)

        val messageToAdd = ChatMessageEntity(
            content = message,
            participantId = destination,
            timestamp = now.time,
            direction = ChatMessageDomain.Direction.SENT.value
        )
        given(chatMessageDao.addMessage(messageToAdd)).willThrow(IllegalArgumentException())

        underTest.sendMessage(message, destination)

        inOrder(messagingService, chatMessageDao).apply {
            verify(chatMessageDao).addMessage(messageToAdd)
            verifyNoInteractions(messagingService)
        }
    }
}
