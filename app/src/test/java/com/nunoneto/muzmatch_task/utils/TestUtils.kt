package com.nunoneto.muzmatch_task.utils

import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity
import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.repo.domain.models.ParticipantDomain
import com.nunoneto.muzmatch_task.ui.models.ChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ParticipantModel
import com.nunoneto.muzmatch_task.ui.models.TimestampChatModel

fun participantEntity() = ParticipantEntity(
    id = "participant_id",
    name = "Sarah Jessica Parker",
    photoUrl = "photo-url"
)

fun chatMessageEntity() = ChatMessageEntity(
    content = "hello!",
    participantId = "participant-id",
    timestamp = 0L,
    direction = ChatMessageDomain.Direction.SENT.value
)

fun participantDomain() = ParticipantDomain(
    name = "Sarah Jessica Parker",
    photoUrl = "photo-url"
)

fun chatMessageDomain() = ChatMessageDomain(
    content = "hello!",
    timestamp = 0L,
    direction = ChatMessageDomain.Direction.SENT
)

fun participantUiModel() = ParticipantModel(
    name = "Sarah Jessica Parker",
    photoUrl = "photo-url"
)

fun timestampSeparatorModel() = TimestampChatModel(
    dayOfWeek = "dayweek",
    timeOfDay = "timeday"
)

fun chatMessageUiModel() = ChatMessageModel(
    isOwnMessage = true,
    showTail = false,
    content = "hello!"
)
