package com.nunoneto.muzmatch_task.utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher

@ExperimentalCoroutinesApi
object UnitTestDispatcher : IDispatcherProvider {

    private val testDispatcher = StandardTestDispatcher()

    override fun background(): CoroutineDispatcher = testDispatcher
    override fun main(): CoroutineDispatcher = testDispatcher
    override fun default() = testDispatcher
}
