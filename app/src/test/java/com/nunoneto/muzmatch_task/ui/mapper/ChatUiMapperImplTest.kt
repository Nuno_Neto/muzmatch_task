package com.nunoneto.muzmatch_task.ui.mapper

import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.utils.DateUtils
import com.nunoneto.muzmatch_task.utils.chatMessageDomain
import com.nunoneto.muzmatch_task.utils.chatMessageUiModel
import com.nunoneto.muzmatch_task.utils.participantDomain
import com.nunoneto.muzmatch_task.utils.participantUiModel
import com.nunoneto.muzmatch_task.utils.timestampSeparatorModel
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class ChatUiMapperImplTest {

    @Mock
    private lateinit var dateUtils: DateUtils

    private lateinit var underTest: IChatUiMapper

    @Before
    fun setup() {
        underTest = ChatUiMapperImpl(dateUtils)
    }

    @Test
    fun `given participant domain, should map to participant ui model`() {
        val actual = underTest.mapParticipant(participantDomain())

        assertEquals(participantUiModel(), actual)
    }

    @Test
    fun `given received domain message list, should return timestamp separator and received ui messages`() {
        givenDateUtilsReturns()

        val actual = underTest.mapChatMessages(listOf(
            chatMessageDomain().copy(direction = ChatMessageDomain.Direction.RECEIVED)
        ))

        assertEquals(
            listOf(
                timestampSeparatorModel(),
                chatMessageUiModel().copy(isOwnMessage = false, showTail = true)
            ),
            actual
        )
    }

    @Test
    fun `given sent domain message list, should return timestamp separator and sent ui messages`() {
        givenDateUtilsReturns()

        val actual = underTest.mapChatMessages(listOf(
            chatMessageDomain()
        ))

        assertEquals(
            listOf(
                timestampSeparatorModel(),
                chatMessageUiModel().copy(showTail = true)
            ),
            actual
        )
    }

    @Test
    fun `given messages more than a hour in between, should add timestamp separator`() {
        given(dateUtils.getDayOfWeek(0)).willReturn("dayweek")
        given(dateUtils.getTimeOfDay(0)).willReturn("timeday")

        val timestamp = TimeUnit.MINUTES.toMillis(61)
        given(dateUtils.getDayOfWeek(timestamp)).willReturn("dayweek2")
        given(dateUtils.getTimeOfDay(timestamp)).willReturn("timeday2")

        val actual = underTest.mapChatMessages(listOf(
            chatMessageDomain().copy(timestamp = 0),
            chatMessageDomain().copy(timestamp = TimeUnit.MINUTES.toMillis(61))
        ))

        assertEquals(
            listOf(
                timestampSeparatorModel(),
                chatMessageUiModel().copy(isOwnMessage = true, showTail = true),
                timestampSeparatorModel().copy("dayweek2", "timeday2"),
                chatMessageUiModel().copy(isOwnMessage = true, showTail = true)
            ),
            actual
        )
    }

    @Test
    fun `given message after is sent more than 20 seconds after, message should show tail`() {
        givenDateUtilsReturns()

        val actual = underTest.mapChatMessages(listOf(
            chatMessageDomain().copy(direction = ChatMessageDomain.Direction.RECEIVED, timestamp = 0),
            chatMessageDomain().copy(direction = ChatMessageDomain.Direction.RECEIVED, timestamp = TimeUnit.SECONDS.toMillis(21))
        ))

        assertEquals(
            listOf(
                timestampSeparatorModel(),
                chatMessageUiModel().copy(isOwnMessage = false, showTail = true),
                chatMessageUiModel().copy(isOwnMessage = false, showTail = true)
            ),
            actual
        )
    }

    @Test
    fun `given message after is from the other user, message should show tail`() {
        givenDateUtilsReturns()

        val actual = underTest.mapChatMessages(listOf(
            chatMessageDomain().copy(direction = ChatMessageDomain.Direction.RECEIVED),
            chatMessageDomain().copy(direction = ChatMessageDomain.Direction.SENT),
        ))

        assertEquals(
            listOf(
                timestampSeparatorModel(),
                chatMessageUiModel().copy(isOwnMessage = false, showTail = true),
                chatMessageUiModel().copy(isOwnMessage = true, showTail = true)
            ),
            actual
        )
    }

    private fun givenDateUtilsReturns() {
        given(dateUtils.getDayOfWeek(0)).willReturn("dayweek")
        given(dateUtils.getTimeOfDay(0)).willReturn("timeday")
    }
}
