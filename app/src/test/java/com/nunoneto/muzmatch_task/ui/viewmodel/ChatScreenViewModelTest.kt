package com.nunoneto.muzmatch_task.ui.viewmodel

import androidx.lifecycle.SavedStateHandle
import com.nunoneto.muzmatch_task.repo.IChatMessageRepository
import com.nunoneto.muzmatch_task.ui.mapper.IChatUiMapper
import com.nunoneto.muzmatch_task.ui.models.BaseChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ParticipantModel
import com.nunoneto.muzmatch_task.ui.screens.ChatScreenFragment
import com.nunoneto.muzmatch_task.utils.CoroutineTestRule
import com.nunoneto.muzmatch_task.utils.UnitTestDispatcher
import com.nunoneto.muzmatch_task.utils.chatMessageDomain
import com.nunoneto.muzmatch_task.utils.chatMessageUiModel
import com.nunoneto.muzmatch_task.utils.participantDomain
import com.nunoneto.muzmatch_task.utils.participantUiModel
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.inOrder
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ChatScreenViewModelTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Mock
    private lateinit var repository: IChatMessageRepository

    @Mock
    private lateinit var savedStateHandle: SavedStateHandle

    @Mock
    private lateinit var uiMapper: IChatUiMapper

    private lateinit var underTest: ChatScreenViewModel

    @Before
    fun setup() {
        underTest = ChatScreenViewModel(
            repository, savedStateHandle, uiMapper, UnitTestDispatcher
        )
    }

    @Test
    fun `given repo returns participant, should expose to view`() = runTest {
        val participantId = "participant-id"
        given(savedStateHandle.get<String>(ChatScreenFragment.BUNDLE_PARTICIPANT_ID)).willReturn(participantId)

        given(repository.getParticipant(participantId)).willReturn(flowOf(participantDomain()))
        given(uiMapper.mapParticipant(participantDomain())).willReturn(participantUiModel())

        val actual = mutableListOf<ParticipantModel?>()
        underTest.participant.take(1).toList(actual)

        assertEquals(participantUiModel(), actual.first())

        inOrder(savedStateHandle, uiMapper, repository).apply {
            verify(savedStateHandle).get<String>(ChatScreenFragment.BUNDLE_PARTICIPANT_ID)
            verify(repository).getParticipant(participantId)
            verify(uiMapper).mapParticipant(participantDomain())
        }
    }

    @Test
    fun `given repo returns messages, should expose to view`() = runTest {
        val participantId = "participant-id"
        given(savedStateHandle.get<String>(ChatScreenFragment.BUNDLE_PARTICIPANT_ID)).willReturn(participantId)

        given(repository.observeChatMessages(participantId)).willReturn(flowOf(listOf(chatMessageDomain())))
        given(uiMapper.mapChatMessages(listOf(chatMessageDomain()))).willReturn(listOf(chatMessageUiModel()))

        val actual = mutableListOf<List<BaseChatMessageModel>>()
        underTest.chatMessageMessages.take(1).toList(actual)

        assertEquals(listOf(chatMessageUiModel()), actual.first())

        inOrder(savedStateHandle, uiMapper, repository).apply {
            verify(savedStateHandle).get<String>(ChatScreenFragment.BUNDLE_PARTICIPANT_ID)
            verify(repository).observeChatMessages(participantId)
            verify(uiMapper).mapChatMessages(listOf(chatMessageDomain()))
        }
    }

    @Test
    fun `given user sends message, should call repository with appropriate params`() = runTest {
        val participantId = "participant-id"
        given(savedStateHandle.get<String>(ChatScreenFragment.BUNDLE_PARTICIPANT_ID)).willReturn(participantId)

        val message = "hello!"
        launch {
            underTest.sendMessage(message)
        }.join()

        inOrder(savedStateHandle, repository).apply {
            verify(savedStateHandle).get<String>(ChatScreenFragment.BUNDLE_PARTICIPANT_ID)
            verify(repository).sendMessage(message, participantId)
        }
    }
}
