package com.nunoneto.muzmatch_task.ui.mapper

import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.repo.domain.models.ParticipantDomain
import com.nunoneto.muzmatch_task.ui.models.BaseChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ParticipantModel

interface IChatUiMapper {
    fun mapParticipant(participantDomain: ParticipantDomain): ParticipantModel
    fun mapChatMessages(messages: List<ChatMessageDomain>): List<BaseChatMessageModel>
}
