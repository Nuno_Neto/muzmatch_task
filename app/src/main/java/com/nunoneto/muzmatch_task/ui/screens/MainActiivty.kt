package com.nunoneto.muzmatch_task.ui.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(
                    android.R.id.content,
                    ChatScreenFragment.newInstance("sarah_123"),
                    ChatScreenFragment::class.simpleName)
                .commit()
        }
    }
}
