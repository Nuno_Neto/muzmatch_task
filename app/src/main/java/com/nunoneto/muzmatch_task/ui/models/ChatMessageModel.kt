package com.nunoneto.muzmatch_task.ui.models

data class ChatMessageModel(
    val isOwnMessage: Boolean,
    val showTail: Boolean,
    val content: String,
): BaseChatMessageModel
