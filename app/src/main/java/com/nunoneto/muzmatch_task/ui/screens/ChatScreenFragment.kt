package com.nunoneto.muzmatch_task.ui.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults.buttonColors
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults.outlinedTextFieldColors
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import coil.compose.AsyncImage
import com.nunoneto.muzmatch_task.R
import com.nunoneto.muzmatch_task.ui.components.ChatBubble
import com.nunoneto.muzmatch_task.ui.components.TimestampSeparator
import com.nunoneto.muzmatch_task.ui.models.BaseChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ParticipantModel
import com.nunoneto.muzmatch_task.ui.models.TimestampChatModel
import com.nunoneto.muzmatch_task.ui.viewmodel.ChatScreenViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ChatScreenFragment : Fragment() {

    private val viewModel: ChatScreenViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return ComposeView(requireContext()).apply {
            setContent {

                Scaffold(
                    modifier = Modifier.fillMaxSize(),
                    topBar = {
                        TopBar(viewModel.participant)
                    },
                    content = {
                        Column(
                            modifier = Modifier.fillMaxSize(),
                        ) {
                            MessageList(modifier = Modifier.weight(1f), viewModel)
                            BottomBar(viewModel = viewModel)
                        }
                    }
                )
            }
        }
    }

    companion object {
        const val BUNDLE_PARTICIPANT_ID = "BUNDLE_PARTICIPANT_ID"

        fun newInstance(participantId: String) = ChatScreenFragment().apply {
            arguments = Bundle().apply {
                putString(BUNDLE_PARTICIPANT_ID, participantId)
            }
        }
    }
}

@Composable
fun TopBar(participant: StateFlow<ParticipantModel?>) = TopAppBar(
    title = {
        Row(modifier = Modifier.fillMaxHeight(0.6f)) {
            val state = participant.collectAsState()

            AsyncImage(
                model = state.value?.photoUrl.orEmpty(),
                contentDescription = stringResource(id = R.string.content_description_avatar),
                contentScale = ContentScale.FillHeight,
                placeholder = painterResource(R.drawable.avatar),
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .aspectRatio(1f)
                    .clip(CircleShape)
            )
            Text(text = state.value?.name.orEmpty(),
                modifier = Modifier
                    .padding(start = 8.dp)
                    .align(Alignment.CenterVertically))
        }
    },
    navigationIcon = {
        IconButton(
            modifier = Modifier.fillMaxHeight(),
            onClick = {}) {
            Icon(
                imageVector = Icons.Filled.KeyboardArrowLeft,
                contentDescription = stringResource(id = R.string.content_description_back_arrow),
                tint = colorResource(id = R.color.muzpink),
            )
        }
    },
    backgroundColor = White,
    elevation = 16.dp
)

@Composable
fun MessageList(modifier: Modifier, viewModel: ChatScreenViewModel) =
    Box(modifier = modifier) {

        val scope = rememberCoroutineScope()
        val scrollPosition = rememberSaveable { mutableStateOf(0) }
        val listState = rememberLazyListState()
        val messagesState = viewModel.chatMessageMessages.collectAsState()

        LazyColumn(
            state = listState,
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
        ) {
            items(messagesState.value.count()) {
                when (val item = messagesState.value[it]) {
                    is ChatMessageModel ->
                        ChatBubble(isOwnMessage = item.isOwnMessage, message = item.content, showTail = item.showTail)
                    is TimestampChatModel ->
                        TimestampSeparator(dayOfWeek = item.dayOfWeek, timeOfDay = item.timeOfDay)
                }
            }

            updateListScrollPosition(messagesState, scrollPosition, scope, listState)
        }
        Box(
            modifier = Modifier
                .background(
                    brush = Brush.verticalGradient(
                        colors = listOf(
                            White,
                            Color.LightGray.copy(alpha = 0.2f)
                        )
                    )
                )
                .fillMaxWidth()
                .height(10.dp)
                .align(Alignment.BottomCenter)
        )
    }

private fun updateListScrollPosition(messagesState: State<List<BaseChatMessageModel>>, scrollPosition: MutableState<Int>, scope: CoroutineScope, listState: LazyListState) {
    val size = messagesState.value.size
    if (size > 0 && size != scrollPosition.value) {
        scrollPosition.value = size
        scope.launch {
            listState.scrollToItem(scrollPosition.value)
        }
    }
}

@Composable
fun BottomBar(viewModel: ChatScreenViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(16.dp)
    ) {
        var text by rememberSaveable { mutableStateOf("") }
        OutlinedTextField(
            modifier = Modifier
                .weight(1f)
                .align(Alignment.CenterVertically),
            value = text,
            shape = CircleShape,
            maxLines = 3,
            singleLine = false,
            colors = outlinedTextFieldColors(
                focusedBorderColor = colorResource(id = R.color.muzpink),
                unfocusedBorderColor = Color.LightGray
            ),
            onValueChange = {
                text = it
            },
        )
        Spacer(modifier = Modifier.width(16.dp))
        Button(
            modifier = Modifier
                .size(48.dp)
                .align(Alignment.CenterVertically),
            shape = CircleShape,
            colors = buttonColors(backgroundColor = colorResource(id = R.color.muzpink), contentColor = White),
            contentPadding = PaddingValues(0.dp),
            enabled = text.isNotBlank(),
            onClick = {
                viewModel.sendMessage(text)
                text = ""
            }) {
            Icon(
                imageVector = Icons.Filled.Send,
                contentDescription = stringResource(id = R.string.content_description_send_button),
            )
        }
    }
}

