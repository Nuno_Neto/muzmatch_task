package com.nunoneto.muzmatch_task.ui.mapper

import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.repo.domain.models.ParticipantDomain
import com.nunoneto.muzmatch_task.ui.models.BaseChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ParticipantModel
import com.nunoneto.muzmatch_task.ui.models.TimestampChatModel
import com.nunoneto.muzmatch_task.utils.DateUtils
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ChatUiMapperImpl @Inject constructor(
    private val dateUtils: DateUtils,
) : IChatUiMapper {

    override fun mapParticipant(participantDomain: ParticipantDomain) =
        ParticipantModel(
            name = participantDomain.name,
            photoUrl = participantDomain.photoUrl
        )

    override fun mapChatMessages(messages: List<ChatMessageDomain>): List<BaseChatMessageModel> = messages.flatMap {
        listOfNotNull(
            itemSection(it, messages),
            chatMessage(it, messages)
        )
    }

    private fun itemSection(currentMessage: ChatMessageDomain, messages: List<ChatMessageDomain>): TimestampChatModel? =
        currentMessage.takeIf {
            messages.firstOrNull() == it ||
                previousMessage(currentMessage, messages)?.run { currentMessage.timestamp - timestamp > TimeUnit.HOURS.toMillis(1) } == true
        }?.run {
            TimestampChatModel(
                dayOfWeek = dateUtils.getDayOfWeek(timestamp),
                timeOfDay = dateUtils.getTimeOfDay(timestamp)
            )
        }

    private fun chatMessage(currentMessage: ChatMessageDomain, messages: List<ChatMessageDomain>) = ChatMessageModel(
        content = currentMessage.content,
        isOwnMessage = currentMessage.direction == ChatMessageDomain.Direction.SENT,
        showTail = shouldShowTail(currentMessage, messages)
    )

    private fun shouldShowTail(currentMessage: ChatMessageDomain, messages: List<ChatMessageDomain>): Boolean =
        messages.lastOrNull() == currentMessage ||                                                                  // last message condition
            nextMessage(currentMessage, messages)?.run {
                direction != currentMessage.direction ||                                                            // next message is from different user
                    TimeUnit.MILLISECONDS.toSeconds(timestamp - currentMessage.timestamp) > 20              // next message is more than 20s after condition
            } == true

    private fun nextMessage(message: ChatMessageDomain, messages: List<ChatMessageDomain>): ChatMessageDomain? =
        messages.elementAtOrNull(messages.indexOf(message).inc())

    private fun previousMessage(message: ChatMessageDomain, messages: List<ChatMessageDomain>): ChatMessageDomain? =
        messages.elementAtOrNull(messages.indexOf(message).dec())
}
