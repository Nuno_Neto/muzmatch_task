package com.nunoneto.muzmatch_task.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import com.nunoneto.muzmatch_task.R

@Composable
fun TimestampSeparator(dayOfWeek: String, timeOfDay: String) {
    val annotatedString = buildAnnotatedString {
        withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, color = Color.Gray)) {
            append(dayOfWeek)
        }
        append(" ")
        withStyle(style = SpanStyle(color = Color.LightGray)) {
            append(timeOfDay)
        }
    }

    return Text(
        text = annotatedString,
        modifier = Modifier
            .padding(vertical = 8.dp)
            .fillMaxWidth(),
        textAlign = TextAlign.Center,
    )
}

@Composable
fun ChatBubble(
    isOwnMessage: Boolean,
    message: String,
    showTail: Boolean,
) = Row(
    horizontalArrangement = getBubbleAlignment(isOwnMessage),
    modifier = Modifier
        .fillMaxWidth()
        .padding(vertical = 8.dp)
) {
    Box(
        modifier = Modifier
            .fillMaxWidth(0.7f)
            .background(
                shape = getBubbleShape(isOwnMessage),
                color = getBubbleBackgroundColor(isOwnMessage)
            )
            .padding(8.dp),
    ) {
        Text(
            text = message,
            color = getMessageTextColor(isOwnMessage),
        )
        if (showTail) {
            Icon(
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .size(12.dp),
                tint = Color.LightGray,
                imageVector = Icons.Filled.Check,
                contentDescription = "tail"
            )
        }
    }
}

private fun getMessageTextColor(isOwnMessage: Boolean) =
    if (isOwnMessage) Color.White else Color.Black

private fun getBubbleAlignment(isOwnMessage: Boolean) =
    if (isOwnMessage) Arrangement.End else Arrangement.Start

private fun getBubbleShape(isOwnMessage: Boolean) =
    RoundedCornerShape(
        topStart = 8.dp,
        topEnd = 8.dp,
        bottomStart = if (isOwnMessage) 8.dp else 0.dp,
        bottomEnd = if (isOwnMessage) 0.dp else 8.dp
    )

@Composable
private fun getBubbleBackgroundColor(isOwnMessage: Boolean) =
    if (isOwnMessage) colorResource(id = R.color.muzpink) else Color.LightGray
