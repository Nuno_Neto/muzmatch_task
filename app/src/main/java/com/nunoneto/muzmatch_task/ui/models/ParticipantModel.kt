package com.nunoneto.muzmatch_task.ui.models

data class ParticipantModel(
    val name: String,
    val photoUrl: String
)
