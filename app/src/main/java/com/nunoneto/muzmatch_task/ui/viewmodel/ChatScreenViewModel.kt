package com.nunoneto.muzmatch_task.ui.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nunoneto.muzmatch_task.repo.IChatMessageRepository
import com.nunoneto.muzmatch_task.ui.mapper.IChatUiMapper
import com.nunoneto.muzmatch_task.ui.models.BaseChatMessageModel
import com.nunoneto.muzmatch_task.ui.models.ParticipantModel
import com.nunoneto.muzmatch_task.ui.screens.ChatScreenFragment.Companion.BUNDLE_PARTICIPANT_ID
import com.nunoneto.muzmatch_task.utils.IDispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChatScreenViewModel @Inject constructor(
    private val repository: IChatMessageRepository,
    private val savedStateHandle: SavedStateHandle,
    private val uiMapper: IChatUiMapper,
    private val dispatcherProvider: IDispatcherProvider,
) : ViewModel() {

    private val participantId: String
        get() = savedStateHandle.get<String>(BUNDLE_PARTICIPANT_ID).orEmpty()

    val participant: StateFlow<ParticipantModel?> by lazy {
        repository.getParticipant(participantId)
            .map {
                uiMapper.mapParticipant(it)
            }
            .flowOn(dispatcherProvider.background())
            .stateIn(viewModelScope, SharingStarted.Lazily, null)
    }

    val chatMessageMessages: StateFlow<List<BaseChatMessageModel>> by lazy {
        repository.observeChatMessages(participantId)
            .map {
                uiMapper.mapChatMessages(it)
            }
            .flowOn(dispatcherProvider.background())
            .stateIn(viewModelScope, SharingStarted.Lazily, emptyList())
    }

    fun sendMessage(message: String) {
        viewModelScope.launch(context = dispatcherProvider.background()) {
            kotlin.runCatching {
                repository.sendMessage(message, participantId)
            }
        }
    }
}

