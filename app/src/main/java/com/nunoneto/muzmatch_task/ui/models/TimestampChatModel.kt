package com.nunoneto.muzmatch_task.ui.models

data class TimestampChatModel(
    val dayOfWeek: String,
    val timeOfDay: String,
) : BaseChatMessageModel
