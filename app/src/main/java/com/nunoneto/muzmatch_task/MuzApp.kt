package com.nunoneto.muzmatch_task

import android.app.Application
import com.nunoneto.muzmatch_task.db.MuzDatabase
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity
import com.nunoneto.muzmatch_task.utils.IDispatcherProvider
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltAndroidApp
class MuzApp: Application() {

    @Inject
    lateinit var muzDatabase: MuzDatabase

    @Inject
    lateinit var coroutineDispatcher: IDispatcherProvider

    override fun onCreate() {
        super.onCreate()

        setupTestParticipant()
    }

    private fun setupTestParticipant() {
        MainScope().launch {
            withContext(coroutineDispatcher.background()) {
                muzDatabase.participantDao().insert(ParticipantEntity(
                    id = "sarah_123",
                    name = "Sarah Jessica Parker",
                    photoUrl = "https://jpimg.com.br/uploads/2022/02/design-sem-nome-23.jpg"
                ))
            }
        }
    }
}
