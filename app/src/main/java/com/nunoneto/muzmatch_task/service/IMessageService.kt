package com.nunoneto.muzmatch_task.service

interface IMessageService {
    suspend fun sendMessage(message: String, destinationId: String)
}
