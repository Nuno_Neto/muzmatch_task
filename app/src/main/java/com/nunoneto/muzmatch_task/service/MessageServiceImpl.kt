package com.nunoneto.muzmatch_task.service

import com.nunoneto.muzmatch_task.db.MuzDatabase
import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import kotlinx.coroutines.delay
import java.util.Date
import javax.inject.Inject
import kotlin.random.Random

class MessageServiceImpl @Inject constructor(
    private val muzDatabase: MuzDatabase,
) : IMessageService {
    override suspend fun sendMessage(message: String, destinationId: String) {
        // Simulate sending message to remote endpoint
        delay(2000)
        simulateParticipantResponse(destinationId)
    }

    private fun simulateParticipantResponse(destinationId: String) {
        repeat((0..Random.nextInt(0, 3)).count()) {
            muzDatabase.chatMessageDao().addMessage(
                ChatMessageEntity(
                    content = getRandomMessage(),
                    participantId = destinationId,
                    direction = ChatMessageDomain.Direction.RECEIVED.value,
                    timestamp = Date().time
                )
            )
        }
    }

    private fun getRandomMessage(): String {
        val responses = listOf(
            "That's great!",
            "Hey hey!",
            "Booooh",
            "When do you want to meet up?",
            "Booooring",
            "Whatcha doing?",
            "Ready yet?",
            "What's your favorite restaurant?"
        )

        return responses.elementAt(Random.nextInt(0, responses.size.dec()))
    }
}
