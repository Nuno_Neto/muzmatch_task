package com.nunoneto.muzmatch_task.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ParticipantDao {

    @Insert(onConflict = IGNORE)
    abstract fun insert(participant: ParticipantEntity)

    @Query(QUERY_PARTICIPANT_BY_ID)
    abstract fun getParticipant(id: String): Flow<ParticipantEntity>

    companion object {
        private const val QUERY_PARTICIPANT_BY_ID = "SELECT * FROM ${ParticipantEntity.TABLE_NAME} WHERE ${ParticipantEntity.ID} like :id"
    }
}
