package com.nunoneto.muzmatch_task.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class ParticipantEntity(
    @PrimaryKey
    @ColumnInfo(name = ID)
    val id: String,
    @ColumnInfo(name = NAME)
    val name: String,
    @ColumnInfo(name = PHOTO)
    val photoUrl: String
) {
    companion object {
        const val TABLE_NAME = "participant"

        const val ID = "participant_id"
        private const val NAME = "participant_name"
        private const val PHOTO = "participant_photo_url"
    }
}
