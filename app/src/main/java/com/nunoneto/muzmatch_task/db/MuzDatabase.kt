package com.nunoneto.muzmatch_task.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nunoneto.muzmatch_task.db.dao.ChatMessageDao
import com.nunoneto.muzmatch_task.db.dao.ParticipantDao
import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity

@Database(entities = [ParticipantEntity::class, ChatMessageEntity::class], version = 1)
abstract class MuzDatabase : RoomDatabase() {

    abstract fun participantDao(): ParticipantDao
    abstract fun chatMessageDao(): ChatMessageDao

    companion object {
        private const val DATABASE_NAME = "muz_database"

        fun build(applicationContext: Context) = Room.databaseBuilder(
            applicationContext,
            MuzDatabase::class.java, DATABASE_NAME
        ).build()
    }
}
