package com.nunoneto.muzmatch_task.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ChatMessageDao {

    @Query(QUERY_MSG_BY_PARTICIPANT)
    abstract fun observeMessagesForParticipant(participantId: String): Flow<List<ChatMessageEntity>>

    @Insert
    abstract fun addMessage(message: ChatMessageEntity)

    companion object {
        private const val QUERY_MSG_BY_PARTICIPANT = "SELECT * FROM ${ChatMessageEntity.TABLE_NAME} WHERE ${ChatMessageEntity.PARTICIPANT} like :participantId"
    }
}
