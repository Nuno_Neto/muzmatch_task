package com.nunoneto.muzmatch_task.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity.Companion.TABLE_NAME

@Entity(
    tableName = TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = ParticipantEntity::class,
            parentColumns = [ParticipantEntity.ID], childColumns = [ChatMessageEntity.PARTICIPANT],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class ChatMessageEntity(
    @ColumnInfo(name = CONTENT)
    val content: String,
    @ColumnInfo(name = TIMESTAMP)
    val timestamp: Long,
    @ColumnInfo(name = PARTICIPANT, index = true)
    val participantId: String,
    @ColumnInfo(name = DIRECTION)
    val direction: Int,
) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ID)
    var id: Int = 0

    companion object {
        const val TABLE_NAME = "message"

        const val ID = "message_id"
        private const val CONTENT = "message_content"
        private const val TIMESTAMP = "message_timestamp"
        const val PARTICIPANT = "message_participant_id"
        private const val DIRECTION = "message_direction"
    }
}
