package com.nunoneto.muzmatch_task.di

import com.nunoneto.muzmatch_task.utils.CoroutineDispatcher
import com.nunoneto.muzmatch_task.utils.IDispatcherProvider
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DispatcherModule {

    @Binds
    fun providesDispatcher(dispatcher: CoroutineDispatcher): IDispatcherProvider
}
