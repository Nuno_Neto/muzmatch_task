package com.nunoneto.muzmatch_task.di

import com.nunoneto.muzmatch_task.repo.ChatMessageRepositoryImpl
import com.nunoneto.muzmatch_task.repo.IChatMessageRepository
import com.nunoneto.muzmatch_task.repo.domain.DomainMapperImpl
import com.nunoneto.muzmatch_task.repo.domain.IDomainMapper
import com.nunoneto.muzmatch_task.service.IMessageService
import com.nunoneto.muzmatch_task.service.MessageServiceImpl
import com.nunoneto.muzmatch_task.ui.mapper.ChatUiMapperImpl
import com.nunoneto.muzmatch_task.ui.mapper.IChatUiMapper
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
interface ChatMessageModule {

    @Binds
    fun providesChatMessageRepository(
        impl: ChatMessageRepositoryImpl,
    ): IChatMessageRepository

    @Binds
    fun providesChatDomainMapper(
        impl: DomainMapperImpl,
    ): IDomainMapper

    @Binds
    fun providesUiMapper(
        impl: ChatUiMapperImpl,
    ): IChatUiMapper

    @Binds
    fun providesMessageService(
        impl: MessageServiceImpl,
    ): IMessageService
}
