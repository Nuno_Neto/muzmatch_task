package com.nunoneto.muzmatch_task.di

import android.content.Context
import com.nunoneto.muzmatch_task.db.MuzDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Singleton
    @Provides
    fun providesDatabase(@ApplicationContext context: Context): MuzDatabase =
        MuzDatabase.build(context)
}
