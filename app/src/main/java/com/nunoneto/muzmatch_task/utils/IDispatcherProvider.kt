package com.nunoneto.muzmatch_task.utils

import kotlinx.coroutines.CoroutineDispatcher

interface IDispatcherProvider {
    fun background(): CoroutineDispatcher
    fun main(): CoroutineDispatcher
    fun default(): CoroutineDispatcher
}
