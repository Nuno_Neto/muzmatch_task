package com.nunoneto.muzmatch_task.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.Date
import javax.inject.Inject

@SuppressLint("SimpleDateFormat")
class DateUtils @Inject constructor() {

    fun getTimeOfDay(timestamp: Long): String =
        SimpleDateFormat("HH:mm").format(Date(timestamp))

    fun getDayOfWeek(timestamp: Long): String =
        SimpleDateFormat("EEE").format(Date(timestamp))

    fun getNow() = Date()
}
