package com.nunoneto.muzmatch_task.repo

import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.repo.domain.models.ParticipantDomain
import kotlinx.coroutines.flow.Flow

interface IChatMessageRepository {
    fun getParticipant(participantId: String): Flow<ParticipantDomain>
    fun observeChatMessages(participantId: String): Flow<List<ChatMessageDomain>>
    suspend fun sendMessage(message: String, destinationParticipantId: String)
}
