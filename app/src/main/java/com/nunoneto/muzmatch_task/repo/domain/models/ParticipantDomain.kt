package com.nunoneto.muzmatch_task.repo.domain.models

data class ParticipantDomain(
    val name: String,
    val photoUrl: String
)
