package com.nunoneto.muzmatch_task.repo.domain

import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity
import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.repo.domain.models.ParticipantDomain

interface IDomainMapper {
    fun mapParticipant(participantEntity: ParticipantEntity): ParticipantDomain
    fun mapChatMessage(chatMessageEntity: ChatMessageEntity): ChatMessageDomain
}
