package com.nunoneto.muzmatch_task.repo

import com.nunoneto.muzmatch_task.db.MuzDatabase
import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import com.nunoneto.muzmatch_task.repo.domain.IDomainMapper
import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.service.IMessageService
import com.nunoneto.muzmatch_task.utils.DateUtils
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ChatMessageRepositoryImpl @Inject constructor(
    private val muzDatabase: MuzDatabase,
    private val domainMapper: IDomainMapper,
    private val dateUtils: DateUtils,
    private val messagingService: IMessageService,
) : IChatMessageRepository {

    override fun getParticipant(participantId: String) =
        muzDatabase.participantDao().getParticipant(participantId).map {
            domainMapper.mapParticipant(it)
        }.catch {}

    override fun observeChatMessages(participantId: String) =
        muzDatabase.chatMessageDao().observeMessagesForParticipant(participantId).map { messages ->
            messages.map {
                domainMapper.mapChatMessage(it)
            }
        }.catch { emit(emptyList()) }

    override suspend fun sendMessage(message: String, destinationParticipantId: String) {
        muzDatabase.chatMessageDao().addMessage(ChatMessageEntity(
            content = message,
            timestamp = dateUtils.getNow().time,
            participantId = destinationParticipantId,
            direction = ChatMessageDomain.Direction.SENT.value
        ))

        messagingService.sendMessage(message, destinationParticipantId)
    }
}
