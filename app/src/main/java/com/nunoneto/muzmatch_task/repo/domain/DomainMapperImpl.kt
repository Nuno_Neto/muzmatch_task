package com.nunoneto.muzmatch_task.repo.domain

import com.nunoneto.muzmatch_task.db.entities.ChatMessageEntity
import com.nunoneto.muzmatch_task.db.entities.ParticipantEntity
import com.nunoneto.muzmatch_task.repo.domain.models.ChatMessageDomain
import com.nunoneto.muzmatch_task.repo.domain.models.ParticipantDomain
import javax.inject.Inject

class DomainMapperImpl @Inject constructor() : IDomainMapper {
    override fun mapParticipant(participantEntity: ParticipantEntity) = ParticipantDomain(
        name = participantEntity.name,
        photoUrl = participantEntity.photoUrl
    )

    override fun mapChatMessage(chatMessageEntity: ChatMessageEntity) = ChatMessageDomain(
        content = chatMessageEntity.content,
        timestamp = chatMessageEntity.timestamp,
        direction = ChatMessageDomain.Direction.values().firstOrNull { it.value == chatMessageEntity.direction }
            ?: ChatMessageDomain.Direction.UNKNOWN
    )
}
