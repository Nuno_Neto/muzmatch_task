package com.nunoneto.muzmatch_task.repo.domain.models

data class ChatMessageDomain(
    val content: String,
    val timestamp: Long,
    val direction: Direction,
) {
    enum class Direction(val value: Int) {
        UNKNOWN(-1),
        RECEIVED(0),
        SENT(1)
    }
}
